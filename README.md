GitLab Program Management Tools
===============================

Tools for managing tickets and producing metrics for GitLab projects.

Dependencies
------------

 * Python 2.7
 * [python-gitlab](https://github.com/gpocentek/python-gitlab)
